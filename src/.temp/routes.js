export default [
  {
    name: "blogHtmlBlogPostTwo",
    path: "/blog-html/blog-post-two",
    component: () => import(/* webpackChunkName: "component--blog-html-blog-post-two" */ "/Applications/MAMP/htdocs/develop/pandora-gridsome/src/pages/blogHtml/blogPostTwo.vue"),
    meta: { isStatic: true }
  },
  {
    name: "blogHtmlBlogPostOne",
    path: "/blog-html/blog-post-one",
    component: () => import(/* webpackChunkName: "component--blog-html-blog-post-one" */ "/Applications/MAMP/htdocs/develop/pandora-gridsome/src/pages/blogHtml/blogPostOne.vue"),
    meta: { isStatic: true }
  },
  {
    name: "blogHtmlBlogPostThree",
    path: "/blog-html/blog-post-three",
    component: () => import(/* webpackChunkName: "component--blog-html-blog-post-three" */ "/Applications/MAMP/htdocs/develop/pandora-gridsome/src/pages/blogHtml/blogPostThree.vue"),
    meta: { isStatic: true }
  },
  {
    name: "team",
    path: "/team",
    component: () => import(/* webpackChunkName: "component--team" */ "/Applications/MAMP/htdocs/develop/pandora-gridsome/src/pages/Team.vue"),
    meta: { isStatic: true }
  },
  {
    name: "home",
    path: "/",
    component: () => import(/* webpackChunkName: "component--home" */ "/Applications/MAMP/htdocs/develop/pandora-gridsome/src/pages/Index.vue")
  },
  {
    name: "about",
    path: "/about",
    component: () => import(/* webpackChunkName: "component--about" */ "/Applications/MAMP/htdocs/develop/pandora-gridsome/src/pages/About.vue"),
    meta: { isStatic: true }
  },
  {
    name: "404",
    path: "/404",
    component: () => import(/* webpackChunkName: "component--404" */ "/Applications/MAMP/htdocs/develop/pandora-gridsome/node_modules/gridsome/app/pages/404.vue"),
    meta: { isIndex: false }
  },
  {
    path: "/blog/another-awesome-post",
    component: () => import(/* webpackChunkName: "component--post" */ "/Applications/MAMP/htdocs/develop/pandora-gridsome/src/templates/Post.vue")
  },
  {
    path: "/blog/blog-post-four",
    component: () => import(/* webpackChunkName: "component--post" */ "/Applications/MAMP/htdocs/develop/pandora-gridsome/src/templates/Post.vue")
  },
  {
    path: "/blog/blog-post-one",
    component: () => import(/* webpackChunkName: "component--post" */ "/Applications/MAMP/htdocs/develop/pandora-gridsome/src/templates/Post.vue")
  },
  {
    path: "/blog/blog-post-three",
    component: () => import(/* webpackChunkName: "component--post" */ "/Applications/MAMP/htdocs/develop/pandora-gridsome/src/templates/Post.vue")
  },
  {
    path: "/blog/blog-post-two",
    component: () => import(/* webpackChunkName: "component--post" */ "/Applications/MAMP/htdocs/develop/pandora-gridsome/src/templates/Post.vue")
  },
  {
    name: "redditPost",
    path: "/reddit/:id",
    component: () => import(/* webpackChunkName: "component--reddit-post" */ "/Applications/MAMP/htdocs/develop/pandora-gridsome/src/templates/RedditPost.vue")
  },
  {
    name: "*",
    path: "*",
    component: () => import(/* webpackChunkName: "component--404" */ "/Applications/MAMP/htdocs/develop/pandora-gridsome/node_modules/gridsome/app/pages/404.vue"),
    meta: { isIndex: false }
  }
]

